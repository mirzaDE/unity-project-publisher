﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs">
//     Copyright(C) 2016
// </copyright>
// <author>Mirza Özokyay</author>
// <summary>Tool for automated publishing of Unity projects</summary>
//-----------------------------------------------------------------------

namespace Unity_Project_Publisher
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Security;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Properties;

    /// <summary>
    /// Available platforms
    /// </summary>
    public enum Platform
    {
        /// <summary>
        /// Windows standalone
        /// </summary>
        Windows,

        /// <summary>
        /// Linux standalone
        /// </summary>
        Linux,

        /// <summary>
        /// WebGL HTML 5
        /// </summary>
        WebGL
    }

    /// <summary>
    /// Available system architectures
    /// </summary>
    /// <remarks>
    /// This option is ignored if the platform is WebGL.
    /// </remarks>
    public enum Architecture
    {
        /// <summary>
        /// Build for the 32-bit architecture
        /// </summary>
        x86,

        /// <summary>
        /// Build for the 64-bit architecture
        /// </summary>
        x64,

        /// <summary>
        /// Build a version that is compatible to both x86- and x64-based systems
        /// </summary>
        x86_64
    }

    /// <summary>
    /// The application's main class
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The password for the server
        /// </summary>
        private static SecureString password;

        /// <summary>
        /// The application's entry point
        /// </summary>
        /// <param name="args">Command line arguments</param>
        private static void Main(string[] args)
        {
            // Set the default exit code to -1 (error) to be prepared for unexpected errors/crashes
            Environment.ExitCode = -1;

            if (args.Length != 0)
            {
                // Automatic mode - arguments are commands
                string commandChain = string.Join("?", args);
                string[] commands = commandChain.Split(new string[] { " ; ", " ;", "; ", ";" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string command in commands)
                {
                    // Try to process the command
                    if (!ProcessCommand(command, true))
                    {
                        // The command could not be processed - exit
                        return;
                    }
                }
            }
            else
            {
                // Normal mode - process input from the command prompt
                Console.WriteLine("Unity Project Publisher");
                Console.WriteLine();

                // Make sure all settings are set
                ValidateSettings();

                // Show them
                ShowSettings();

                Console.WriteLine();
                Console.WriteLine("Ready");

                string input = Console.ReadLine();
                while (input != "exit")
                {
                    // Process all user commands except for exit-command
                    ProcessCommand(input);
                    input = Console.ReadLine();
                }
            }

            // No errors occured - reset the exit code
            Environment.ExitCode = 0;
        }

        /// <summary>
        /// Makes sure that all settings are set and prompts the user if not
        /// </summary>
        /// <returns>If any settings were modified</returns>
        private static bool ValidateSettings()
        {
            bool settingsChanged = false;

            Settings.Default.ProjectDirectory = CheckSetting(
                Settings.Default.ProjectDirectory,
                "Please enter the path to your project's main directory",
                ref settingsChanged);

            Settings.Default.TargetFile = CheckSetting(
                Settings.Default.TargetFile,
                "Please enter your target file location for unity builds (without file extension)",
                ref settingsChanged);

            Settings.Default.OutputDirectory = CheckSetting(
                Settings.Default.OutputDirectory,
                "Please enter the path to your final output directory",
                ref settingsChanged);

            Settings.Default.ServerAddress = CheckSetting(
                Settings.Default.ServerAddress,
                "Please enter your server's address",
                ref settingsChanged,
                "ftp://");

            Settings.Default.ServerUsername = CheckSetting(
                Settings.Default.ServerUsername,
                "Please enter your user name for the server",
                ref settingsChanged);

            Settings.Default.Unity = CheckSetting(
                Settings.Default.Unity,
                "Please enter the path to the main executable of your Unity installation",
                @"Unity\Editor\Unity.exe",
                ref settingsChanged);

            Settings.Default.InnoSetup = CheckSetting(
                Settings.Default.InnoSetup,
                "Please enter the path to your Inno Setup script compiler (Windows builds only)",
                @"Inno Setup 5\ISCC.exe",
                ref settingsChanged);

            Settings.Default.InnoScript = CheckSetting(
                Settings.Default.InnoScript,
                "Please enter the path to your Inno Setup script (Windows builds only)",
                ref settingsChanged);

            Settings.Default.SevenZip = CheckSetting(
                Settings.Default.SevenZip,
                "Please enter the path to the 7z.exe of your 7-Zip installation (Linux builds only)",
                @"7-Zip\7z.exe",
                ref settingsChanged);

            if (settingsChanged)
            {
                // Save the settings if there are any changes
                Settings.Default.Save();
            }

            return settingsChanged;
        }

        /// <summary>
        /// Gets a masked input from the user
        /// </summary>
        /// <returns>The user input as a <see cref="SecureString" /></returns>
        private static SecureString GetPassword()
        {
            ConsoleKeyInfo keyInfo;
            SecureString buffer = new SecureString();

            do
            {
                keyInfo = Console.ReadKey(true);

                if (keyInfo.Key != ConsoleKey.Backspace && keyInfo.Key != ConsoleKey.Enter)
                {
                    buffer.AppendChar(keyInfo.KeyChar);
                    Console.Write("*");
                }
                else if (keyInfo.Key == ConsoleKey.Backspace && buffer.Length > 0)
                {
                    buffer.RemoveAt(buffer.Length - 1);
                    Console.Write("\b \b");
                }
            }
            while (keyInfo.Key != ConsoleKey.Enter || buffer.Length == 0);

            Console.WriteLine();
            return buffer;
        }

        /// <summary>
        /// Asks the user to enter a setting if the setting is null or white space
        /// </summary>
        /// <param name="setting">The setting to check</param>
        /// <param name="message">The message which is written to the console if the setting needs to be modified by the user</param>
        /// <param name="changed">Is set to TRUE only if the setting was modified</param>
        /// <param name="prefix">This string is put before the user input</param>
        /// <returns>The modified version of the setting</returns>
        private static string CheckSetting(string setting, string message, ref bool changed, string prefix = "")
        {
            bool invalid = false;

            while (string.IsNullOrWhiteSpace(setting))
            {
                if (invalid)
                {
                    Console.WriteLine("Invalid input");
                }

                Console.Write("{0}: {1}", message, prefix);
                
                setting = prefix + Console.ReadLine().Replace("\"", string.Empty);

                invalid = true;
                changed = true;
            }

            return setting;
        }

        /// <summary>
        /// Tries to automatically generate the path for a setting and asks the user if it fails
        /// </summary>
        /// <param name="setting">The setting to check</param>
        /// <param name="message">The message which is written to the console if the setting needs to be modified by the user</param>
        /// <param name="programPath">The second part of the path to where the wanted file could be located</param>
        /// <param name="changed">Is set to TRUE only if the setting was modified</param>
        /// <returns>The modified version of the setting</returns>
        private static string CheckSetting(string setting, string message, string programPath, ref bool changed)
        {
            // Check if the setting needs to be changed
            if (string.IsNullOrWhiteSpace(setting))
            {
                // Try to automatically generate the path

                // Possible paths to check
                string[] possibleFileLocations = new string[]
                {
                    // Program Files
                    // Workaround because sometimes 'ProgramFiles' points to 'Program Files (x86)'
                    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles).Replace(" (x86)", string.Empty), programPath),

                    // Program Files (x86)
                    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), programPath)
                };

                // Check them
                foreach (string file in possibleFileLocations)
                {
                    if (File.Exists(file))
                    {
                        // Keep it if it exists
                        changed = true;
                        return file;
                    }
                }

                // Failed to automatically gernerate the path - ask the user
                return CheckSetting(setting, message, ref changed);
            }
            else
            {
                // No changes necessary
                return setting;
            }
        }

        /// <summary>
        /// Checks if the word count is between 'min' and 'max' and gives and error otherwise
        /// </summary>
        /// <param name="min">The smallest allowed count</param>
        /// <param name="max">The highest allowed count</param>
        /// <param name="words">The list to check</param>
        /// <returns>TRUE if the word count is between 'min' and 'max'</returns>
        private static bool CheckWordCount(int min, int max, List<string> words)
        {
            // Check if there are too few words
            if (words.Count < min)
            {
                Console.WriteLine("Too few arguments");
                return false;
            }

            // Check if there are too many words
            if (words.Count > max)
            {
                Console.WriteLine("Too many arguments");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Processes a command
        /// </summary>
        /// <param name="command">The command to process</param>
        /// <param name="dontSplit">If true, the command will only be split once at '?'.</param>
        /// <returns>If the command was processed successfully</returns>
        private static bool ProcessCommand(string command, bool dontSplit = false)
        {
            // Check if the command is white space
            if (string.IsNullOrWhiteSpace(command))
            {
                // Command is invalid
                return false;
            }

            // Split the command string in words
            List<string> words;

            if (!dontSplit && command.Contains('"'))
            {
                string[] rawWords = command.Split('"');
                words = new List<string>();

                // More complicated work to ensure that paths are words e.g. "Path with spaces" would be one word
                for (int i = 0; i < rawWords.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        // Make sure the commands are lower-case
                        rawWords[i] = rawWords[i].ToLower();
                        words = words.Concat(rawWords[i].Split(' ')).ToList();
                    }
                    else
                    {
                        words.Add(rawWords[i]);
                    }
                }
            }
            else if (dontSplit)
            {
                words = new List<string>(command.Split('?'));

                // Make everything except paths lower-case
                for (int i = 0; i < words.Count && i < 2; i++)
                {
                    words[i] = words[i].ToLower();
                }
            }
            else
            {
                words = new List<string>(command.ToLower().Split(' '));
            }

            // Remove white-space-only-words
            for (int i = 0; i < words.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(words[i]))
                {
                    words.RemoveAt(i);
                }
            }

            // Prepare function for 'all'-commands
            Func<Func<bool>, bool> doForEveryPlatformIfNecessary = func =>
            {
                if (CheckWordCount(1, 2, words))
                {
                    if (words.Count == 2)
                    {
                        if (words[1] == "all")
                        {
                            bool result = true;
                            Platform selected = Settings.Default.Platform;

                            foreach (Platform platform in Enum.GetValues(typeof(Platform)))
                            {
                                Console.WriteLine("\n-PLATFORM: {0}-", platform);
                                Settings.Default.Platform = platform;
                                result &= func();
                                if (!result)
                                {
                                    WriteError("An error occured - aborting");
                                    return false;
                                }
                            }

                            Settings.Default.Platform = selected;
                            return result;
                        }
                        else
                        {
                            WriteError("Unknown argument '{0}'", words[1]);
                            return false;
                        }
                    }
                    else
                    {
                        return func();
                    }
                }
                else
                {
                    return false;
                }
            };

            // Process command depending on the first word
            switch (words[0])
            {
                // Set a property
                case "set":
                    if (CheckWordCount(2, 3, words))
                    {
                        return Set(words);
                    }
                    else
                    {
                        Console.WriteLine("Type 'SET HELP' for more information on how to use this command.");
                        return false;
                    }

                // Show the current settings
                case "settings":
                    if (CheckWordCount(1, 1, words))
                    {
                        ShowSettings();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                // Build locally
                case "build":
                    return doForEveryPlatformIfNecessary(Build);

                // Upload to the server
                case "deploy":
                    return doForEveryPlatformIfNecessary(Deploy);

                // Build and deploy
                case "release":
                    // Check the server settings first because the release process should not be interrupted
                    if (CheckServerSettings())
                    {
                        return doForEveryPlatformIfNecessary(() => Build() && Deploy());
                    }
                    else
                    {
                        return false;
                    }

                // Write usage information
                case "help":
                    if (CheckWordCount(1, 1, words))
                    {
                        WriteUsage(words[0]);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                // Command not found
                default:
                    Console.WriteLine("Command '{0}' not found", words[0]);
                    Console.WriteLine("Type 'HELP' for more information on how to use this program.");
                    return false;
            }
        }

        /// <summary>
        /// Modifies the settings
        /// </summary>
        /// <param name="words">All words of the command</param>
        /// <returns>If the modification to the settings was successful</returns>
        private static bool Set(List<string> words)
        {
            // Check all possible arguments except for 'help'
            switch (words[1])
            {
                case "platform":
                case "architecture":
                case "project":
                case "target":
                case "output":
                case "server":
                case "username":
                case "unity":
                case "innosetup":
                case "innoscript":
                case "sevenzip":
                    if (!CheckWordCount(3, 3, words))
                    {
                        return false;
                    }

                    break;

                case "password":
                    if (!CheckWordCount(2, 2, words))
                    {
                        return false;
                    }

                    break;
            }

            // Choose setting
            switch (words[1])
            {
                // The target platform for builds
                case "platform":
                    Platform newPlatform;

                    // Try parsing platform type, ignoring case
                    if (Enum.TryParse(words[2], true, out newPlatform))
                    {
                        Settings.Default.Platform = newPlatform;
                        break;
                    }
                    else
                    {
                        // Platform not found
                        Console.WriteLine("Specified platform '{0}' not found", words[2]);
                        return false;
                    }

                // The target architecture for builds
                case "architecture":
                    Architecture newArchitecture;

                    // Try parsing architecture type, ignoring case
                    if (Enum.TryParse(words[2], true, out newArchitecture))
                    {
                        Settings.Default.Architecture = newArchitecture;
                        break;
                    }
                    else
                    {
                        // Architecture not found
                        Console.WriteLine("Architecture '{0}' not found", words[2]);
                        return false;
                    }

                // Path to the project directory
                case "project":
                    Settings.Default.ProjectDirectory = words[2];
                    break;

                // Path to the target file
                case "target":
                    Settings.Default.TargetFile = words[2];
                    break;

                // Output directory
                case "output":
                    Settings.Default.OutputDirectory = words[2];
                    break;

                // Address of the server
                case "server":
                    Settings.Default.ServerAddress = words[2];
                    break;

                // User name for the server
                case "username":
                    Settings.Default.ServerUsername = words[2];
                    break;

                // Password for the server
                case "password":
                    Console.Write("Enter new password: ");
                    password = GetPassword();
                    break;

                // Path to the main executable of a Unity installation
                case "unity":
                    Settings.Default.Unity = words[2];
                    break;

                // Path to the Inno Setup script compiler
                case "innosetup":
                    Settings.Default.InnoSetup = words[2];
                    break;

                // Path to the Inno Setup Script
                case "innoscript":
                    Settings.Default.InnoScript = words[2];
                    break;

                // Path to '7z.exe' of 7-Zip
                case "sevenzip":
                    Settings.Default.SevenZip = words[2];
                    break;

                // Show help for this command
                case "help":
                    WriteUsage(words[0]);
                    return true;

                // Setting not found
                default:
                    Console.WriteLine("Setting '{0}' not found", words[1]);
                    Console.WriteLine("Type 'SET HELP' for more information on how to use this command.");
                    return false;
            }

            Settings.Default.Save();

            // Setting modification succeeded
            return true;
        }

        /// <summary>
        /// Writes the current settings to the console
        /// </summary>
        private static void ShowSettings()
        {
            Console.WriteLine("-SETTINGS-");
            Console.WriteLine("Platform     {0}", Settings.Default.Platform);
            Console.WriteLine("Architecture {0}", Settings.Default.Architecture);
            Console.WriteLine("Project Dir  {0}", Settings.Default.ProjectDirectory);
            Console.WriteLine("FTP Server   {0}", Settings.Default.ServerAddress);
            Console.WriteLine("Username     {0}", Settings.Default.ServerUsername);
            Console.WriteLine("Password     {0}", password == null ? "NOT SET" : new string('*', password.Length));
            Console.WriteLine("Unity        {0}", Settings.Default.Unity);
            Console.WriteLine("Target File  {0}", Settings.Default.TargetFile);
            Console.WriteLine("Output Dir   {0}", Settings.Default.OutputDirectory);
            Console.WriteLine("Inno Setup   {0}", Settings.Default.InnoSetup);
            Console.WriteLine("Inno Script  {0}", Settings.Default.InnoScript);
            Console.WriteLine("7-Zip        {0}", Settings.Default.SevenZip);
        }

        /// <summary>
        /// Builds the project locally
        /// </summary>
        /// <returns>If the Build succeeded</returns>
        private static bool Build()
        {
            // Check if the project directory is set
            if (string.IsNullOrWhiteSpace(Settings.Default.ProjectDirectory))
            {
                WriteError("Project directory not set");
                return false;
            }

            // Check if the project directory exists
            if (!Directory.Exists(Settings.Default.ProjectDirectory))
            {
                WriteError("Project directory not found");
                return false;
            }

            // Check if the target file is set
            if (string.IsNullOrWhiteSpace(Settings.Default.TargetFile))
            {
                WriteError("Path to target file not set");
                return false;
            }

            // Check if the output directory is set
            if (string.IsNullOrWhiteSpace(Settings.Default.OutputDirectory))
            {
                WriteError("Output directory not set");
                return false;
            }

            // Check if the output directory exists
            if (!Directory.Exists(Settings.Default.OutputDirectory))
            {
                WriteError("Output directory not found");
                return false;
            }

            // Check if the path to a Unity executable is set
            if (string.IsNullOrWhiteSpace(Settings.Default.Unity))
            {
                WriteError("Path to a Unity main executable not set");
                return false;
            }

            // Check if the path is correct
            if (!File.Exists(Settings.Default.Unity))
            {
                WriteError("Unity not found");
                return false;
            }

            // Check platform-specific settings
            switch (Settings.Default.Platform)
            {
                case Platform.Windows:
                    // Check if the path to the Inno Setup script compiler is set
                    if (string.IsNullOrWhiteSpace(Settings.Default.InnoSetup))
                    {
                        WriteError("Path to the Inno Setup script compiler is not set");
                        return false;
                    }

                    // Check if the path to the Inno Setup script compiler is valid
                    if (!File.Exists(Settings.Default.InnoSetup))
                    {
                        WriteError("Inno Setup script compiler not found");
                        return false;
                    }

                    // Check if the selected architecture is available
                    if (Settings.Default.Architecture == Architecture.x86_64)
                    {
                        WriteError("Platform x86_64 ('Universal') is not available for Windows");
                        return false;
                    }

                    break;

                case Platform.Linux:
                    // Check if the path to 7-Zip is set
                    if (string.IsNullOrWhiteSpace(Settings.Default.SevenZip))
                    {
                        WriteError("Path to 7-Zip not set");
                        return false;
                    }

                    // Check if the path is valid
                    if (!File.Exists(Settings.Default.SevenZip))
                    {
                        WriteError("7-Zip not found");
                        return false;
                    }

                    break;
            }

            // Check if Unity is running
            Process[] processes = Process.GetProcessesByName("Unity");
            if (processes.Length != 0)
            {
                // Warn the user
                Console.WriteLine("[WARNING] Please close Unity now");

                // Wait for Unity to close
                foreach (Process proc in processes)
                {
                    proc.WaitForExit();
                }
            }

            // All safety checks done - start
            WriteBuildInfo("STARTED");

            // Function for exit-code-error-string generation
            Func<string, int, string> getExitCodeErrorString = (m, c) => string.Format("{0} (exit code: {1} hex: {2})", m, c, c.ToString("X"));

            // Standard StartInfo
            ProcessStartInfo standardStartInfo = new ProcessStartInfo()
            {
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            // Create a process for Unity
            Process unity = new Process();
            unity.StartInfo.FileName = Settings.Default.Unity;

            // These should be available for later
            string architecture;
            string targetFile;

            // Assemble architecture string and file extension depending on platform
            switch (Settings.Default.Platform)
            {
                case Platform.Windows:
                    architecture = Settings.Default.Architecture == Architecture.x64 ? "64" : string.Empty;
                    targetFile = Settings.Default.TargetFile + ".exe";
                    break;

                case Platform.Linux:
                    architecture = Settings.Default.Architecture == Architecture.x86_64 ? "Universal" : Settings.Default.Architecture == Architecture.x64 ? "64" : "32";
                    targetFile = Settings.Default.TargetFile + "." + Settings.Default.Architecture;
                    break;

                case Platform.WebGL:
                    // Make the compiler happy
                    architecture = string.Empty;
                    targetFile = string.Empty;
                    break;

                default:
                    WriteError("Invalid platform");
                    return false;
            }

            if (Settings.Default.Platform == Platform.WebGL)
            {
                // Use a build script for WebGL since there is no flag
                unity.StartInfo.Arguments = string.Format("-batchmode -projectPath \"{0}\" -executeMethod BuildScript.BuildWebGL -logFile -quit", Settings.Default.ProjectDirectory);
            }
            else
            {
                // Normal command line arguments for windows- and linux builds
                unity.StartInfo.Arguments = string.Format(
                    "-batchmode -projectPath \"{0}\" -build{1}{2}Player \"{3}\" -logFile -quit",
                    Settings.Default.ProjectDirectory,
                    Settings.Default.Platform,
                    architecture,
                    targetFile);
            }

            // Start Unity
            unity.Start();
            WriteBuildInfo("Unity project compilation started");
            unity.WaitForExit();

            if (unity.ExitCode == 0)
            {
                // Build succeeded
                WriteBuildInfo("Project compilation succeeded");
            }
            else
            {
                // Build failed
                WriteError(getExitCodeErrorString("Project compilation failed", unity.ExitCode));
                return false;
            }

            // Pack the files depending on the target platform
            switch (Settings.Default.Platform)
            {
                case Platform.Windows:
                    // Create a process for Inno Setup
                    Process innosetup = new Process();
                    innosetup.StartInfo = standardStartInfo;
                    innosetup.StartInfo.FileName = Settings.Default.InnoSetup;
                    innosetup.StartInfo.Arguments = string.Format("/O\"{0}\" \"{1}\"", Settings.Default.OutputDirectory, Settings.Default.InnoScript);

                    innosetup.OutputDataReceived += (s, e) =>
                    {
                        if (!string.IsNullOrWhiteSpace(e.Data))
                        {
                            Console.WriteLine("[INNO] {0}", e.Data);
                        }
                    };
                    innosetup.ErrorDataReceived += (s, e) =>
                    {
                        if (!string.IsNullOrWhiteSpace(e.Data))
                        {
                            Console.WriteLine("[INNO-ERROR] {0}", e.Data);
                        }
                    };

                    // Start the Inno Setup script compiler
                    innosetup.Start();
                    innosetup.BeginOutputReadLine();
                    innosetup.BeginErrorReadLine();
                    WriteBuildInfo("Inno Setup install script compilation started");
                    innosetup.WaitForExit();

                    if (innosetup.ExitCode == 0)
                    {
                        // Compilation succeeded
                        WriteBuildInfo("Compilation succeeded");
                    }
                    else
                    {
                        // Compilation failed
                        WriteError(getExitCodeErrorString("Compilation failed", innosetup.ExitCode));
                        return false;
                    }

                    break;

                case Platform.Linux:
                    // There is no extension because Settings.Default.TargetFile has no extension (entered by user)
                    string targetFileNameWithoutExtension = Path.GetFileName(Settings.Default.TargetFile);
                    string targetArchive = string.Format("{0}\\{1}.tar", Settings.Default.OutputDirectory, targetFileNameWithoutExtension);

                    // Create a process for 7-Zip
                    Process sevenzip = new Process();
                    sevenzip.StartInfo = standardStartInfo;
                    sevenzip.StartInfo.FileName = Settings.Default.SevenZip;
                    sevenzip.StartInfo.Arguments = string.Format(
                        "a \"{0}\" \"{1}{2}_Data\\\" \"{3}\" -ttar",
                        targetArchive,
                        Settings.Default.TargetFile.Replace(targetFileNameWithoutExtension, string.Empty),
                        targetFileNameWithoutExtension,
                        targetFile);

                    sevenzip.OutputDataReceived += (s, e) =>
                    {
                        if (!string.IsNullOrWhiteSpace(e.Data))
                        {
                            Console.WriteLine("[7-ZIP] {0}", e.Data);
                        }
                    };
                    sevenzip.ErrorDataReceived += (s, e) =>
                    {
                        if (!string.IsNullOrWhiteSpace(e.Data))
                        {
                            Console.WriteLine("[7-ZIP-ERROR] {0}", e.Data);
                        }
                    };

                    // Start 7-Zip and generate .tar
                    sevenzip.Start();
                    sevenzip.BeginOutputReadLine();
                    sevenzip.BeginErrorReadLine();
                    WriteBuildInfo("7-Zip archive generation started");
                    sevenzip.WaitForExit();

                    if (sevenzip.ExitCode == 0)
                    {
                        // Packing succeeded
                        WriteBuildInfo("Tar archive generated successfully");

                        // New arguments for GZip compression
                        sevenzip.StartInfo.Arguments = string.Format("a \"{0}.gz\" \"{0}\" -tgzip", targetArchive);

                        // Start 7-Zip for .gz
                        // CancelOutputRead() and CancelErrorRead() need to be called because the process is restarted
                        sevenzip.CancelOutputRead();
                        sevenzip.CancelErrorRead();
                        sevenzip.Start();
                        sevenzip.BeginOutputReadLine();
                        sevenzip.BeginErrorReadLine();
                        WriteBuildInfo("GZip compression started");
                        sevenzip.WaitForExit();

                        // Compressing succeeded
                        WriteBuildInfo("GZip archive generated successfully");

                        // Check exit code
                        if (sevenzip.ExitCode != 0)
                        {
                            // Something went wrong
                            WriteError(getExitCodeErrorString("Compilation failed", sevenzip.ExitCode));
                            return false;
                        }

                        // Delete .tar
                        File.Delete(targetArchive);
                        WriteBuildInfo("Tar archive deleted");
                    }
                    else
                    {
                        // Packing failed
                        WriteError(getExitCodeErrorString("Tar Archive generation failed", sevenzip.ExitCode));
                        return false;
                    }

                    break;

                case Platform.WebGL:
                    // No further packing required
                    break;

                default:
                    WriteError("Invalid platform");
                    return false;
            }

            // Done
            WriteBuildInfo("DONE");

            return true;
        }

        /// <summary>
        /// Uploads the built project to a server
        /// </summary>
        /// <returns>If the process succeeded</returns>
        private static bool Deploy()
        {
            // Check the settings
            if (!CheckServerSettings())
            {
                // Some required settings are not set
                return false;
            }

            // Platform-dependent checks
            if (Settings.Default.Platform == Platform.Windows && string.IsNullOrWhiteSpace(Settings.Default.InnoScript))
            {
                WriteError("Path to Inno Setup script not set");
                return false;
            }

            // Generate paths to all required files/directories
            string file;
            string[] directories = null;

            switch (Settings.Default.Platform)
            {
                case Platform.Windows:
                    string innoscript = File.ReadAllText(Settings.Default.InnoScript);
                    string outputFileName = innoscript.Split(new string[] { "MyAppExeName" }, StringSplitOptions.None)[1].Split('"')[1];
                    file = Path.Combine(Settings.Default.OutputDirectory, outputFileName);
                    break;

                case Platform.Linux:
                    file = Path.Combine(Settings.Default.OutputDirectory, Path.GetFileName(Settings.Default.TargetFile)) + ".tar.gz";
                    break;

                case Platform.WebGL:
                    directories = new string[]
                    {
                        Path.Combine(Settings.Default.ProjectDirectory, @"Build\Release"),
                        Path.Combine(Settings.Default.ProjectDirectory, @"Build\TemplateData")
                    };
                    file = Path.Combine(Settings.Default.ProjectDirectory, @"Build\index.html");
                    break;

                default:
                    WriteError("Invalid platform");
                    return false;
            }

            // Check if they exist
            if (!File.Exists(file))
            {
                WriteError("File '{0}' not found", file);
                return false;
            }

            if (Settings.Default.Platform == Platform.WebGL)
            {
                foreach (string directory in directories)
                {
                    if (!Directory.Exists(directory))
                    {
                        WriteError("Directory '{0}' not found", directory);
                        return false;
                    }
                }
            }

            // All safety-checks done - start
            WriteDeployInfo("STARTED");

            // Create a NetworkCredential
            NetworkCredential credentials = new NetworkCredential(Settings.Default.ServerUsername, password);

            // Prepare for progress updated
            bool progressUpdating = false;
            Action<double> setProgress = prog => Console.Write(new string('\b', 4) + prog + "%" + new string(' ', 3 - prog.ToString().Length));

            // Initialize a WebClient
            WebClient webClient = new WebClient();
            webClient.Credentials = credentials;
            webClient.UploadProgressChanged += (s, e) =>
            {
                // Avoid conflicts
                if (progressUpdating)
                {
                    return;
                }

                progressUpdating = true;

                // Write new progress percentage
                setProgress(Math.Round((double)e.BytesSent / (double)e.TotalBytesToSend * 100));
                progressUpdating = false;
            };
            webClient.UploadFileCompleted += (s, e) =>
            {
                if (e.Error == null && !progressUpdating)
                {
                    // Make sure displayed progress is 100%
                    progressUpdating = true;
                    setProgress(100);
                    progressUpdating = false;
                }

                Console.CursorVisible = true;
                Console.WriteLine();
                ((AutoResetEvent)e.UserState).Set();
            };

            // Upload the file
            Uri baseAddress = new Uri(Settings.Default.ServerAddress);
            Uri destinationUri = new Uri(baseAddress, Settings.Default.Platform == Platform.WebGL ? "game.html" : Path.GetFileName(file));
            
            if (!UploadFile(file, destinationUri, webClient))
            {
                return false;
            }

            // Upload the other directories for WebGL
            if (Settings.Default.Platform == Platform.WebGL)
            {
                foreach (string directory in directories)
                {
                    if (!UploadDirectory(directory, new Uri(baseAddress, Path.GetFileName(directory) + "/"), webClient))
                    {
                        WriteError("Upload of directory '{0}' failed", directory);
                        return false;
                    }
                }
            }

            // Deploy succeeded
            WriteDeployInfo("DONE");
            return true;
        }

        /// <summary>
        /// Prompts the user if any setting is null or white space
        /// </summary>
        /// <returns>TRUE if all settings (except passwords) are fine</returns>
        private static bool CheckServerSettings()
        {
            // Check first if the server address is set
            if (string.IsNullOrWhiteSpace(Settings.Default.ServerAddress))
            {
                WriteError("Server address not set");
                return false;
            }

            // Check if the username is set
            if (string.IsNullOrWhiteSpace(Settings.Default.ServerUsername))
            {
                WriteError("Username not set");
                return false;
            }

            // Check if the password is set
            if (password == null)
            {
                Console.Write("Enter server password: ");
                password = GetPassword();
            }

            return true;
        }

        /// <summary>
        /// Uploads a file to a server
        /// </summary>
        /// <param name="sourceFile">The file to upload</param>
        /// <param name="destinationUri">The destination for the server</param>
        /// <param name="client">The <see cref="WebClient" /> to use</param>
        /// <returns>TRUE if the operation was successful</returns>
        private static bool UploadFile(string sourceFile, Uri destinationUri, WebClient client)
        {
            Console.Write("[DEPLOY] Uploading '{0}'     ", sourceFile);

            AutoResetEvent fileUpload = new AutoResetEvent(false);
            bool success = true;
            Console.CursorVisible = false;

            try
            {
                client.UploadFileAsync(destinationUri, null, sourceFile, fileUpload);
            }
            catch (Exception ex)
            {
                success = false;
                WriteError(ex.Message);
                WriteError("Upload of file '{0}' failed", sourceFile);
            }

            fileUpload.WaitOne();

            return success;
        }

        /// <summary>
        /// Uploads a directory to a server and cancels the operation if any errors occur
        /// </summary>
        /// <param name="sourceDirectory">The directory to upload</param>
        /// <param name="destinationUri">The destination for the server</param>
        /// <param name="client">The <see cref="WebClient" /> to use</param>
        /// <returns>TRUE if the operation was successful</returns>
        private static bool UploadDirectory(string sourceDirectory, Uri destinationUri, WebClient client)
        {
            WriteDeployInfo("Uploading directory '{0}'", sourceDirectory);

            // Get all files and sub-directories
            string[] files = Directory.GetFiles(sourceDirectory, "*.*");
            string[] subDirectories = Directory.GetDirectories(sourceDirectory);

            // Create the directory on the server
            WriteDeployInfo("Creating directory '{0}'", destinationUri);

            FtpWebRequest directoryRequest = (FtpWebRequest)WebRequest.Create(destinationUri);
            directoryRequest.Credentials = client.Credentials;
            directoryRequest.Method = WebRequestMethods.Ftp.MakeDirectory;

            try
            {
                FtpWebResponse response = (FtpWebResponse)directoryRequest.GetResponse();
                response.Close();
            }
            catch (Exception ex)
            {
                // The directory existed
                ////WriteError(ex.Message);
                ////WriteError("Failed to create directory '{0}'", destinationUri);
                ////return false;
            }

            // Upload all files of this directory
            foreach (string file in files)
            {
                if (!UploadFile(file, new Uri(destinationUri, Path.GetFileName(file)), client))
                {
                    return false;
                }
            }

            // Upload all sub-directories
            foreach (string subDirectory in subDirectories)
            {
                if (!UploadDirectory(subDirectory, new Uri(destinationUri, Path.GetFileName(subDirectory) + "/"), client))
                {
                    WriteError("Upload of directory '{0}' failed", subDirectory);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Writes information to the console that describes how to use this program
        /// </summary>
        /// <param name="command">The command to describe</param>
        private static void WriteUsage(string command)
        {
            switch (command)
            {
                case "set":
                    Console.WriteLine("-USAGE (not case-sensitive)-");
                    Console.WriteLine("SET platform [windows/linux/webgl]");
                    Console.WriteLine("SET architecture [x86/x64/x86_64]");
                    Console.WriteLine("SET project [path to the project's main directory]");
                    Console.WriteLine("SET target [path to your target file for unity builds (without file extension)]");
                    Console.WriteLine("SET output [path to your final output directory]");
                    Console.WriteLine("SET server [address to your FTP server]");
                    Console.WriteLine("SET username [your user name for the server]");
                    Console.WriteLine("SET password");
                    Console.WriteLine("SET unity [path to the main executable of your unity installation]");
                    Console.WriteLine("SET innosetup [path to your Inno Setup script compiler]");
                    Console.WriteLine("SET innoscript [path to your Inno Setup Script]");
                    Console.WriteLine("SET sevenzip [path to '7z.exe' of 7-Zip]");
                    break;

                case "help":
                    Console.WriteLine("-COMMANDS (not case-sensitive)-");
                    Console.WriteLine("SET      Change a setting, see 'SET help' for more information");
                    Console.WriteLine("SETTINGS Show the current settings");
                    Console.WriteLine("BUILD    Build the project locally");
                    Console.WriteLine("DEPLOY   Upload the build to a remote server via FTP");
                    Console.WriteLine("RELEASE  A shorthand for build and deploy");
                    Console.WriteLine("HELP     Show this information");
                    break;
            }
        }

        /// <summary>
        /// Writes text with prefix '[BUILD]' to the console
        /// </summary>
        /// <param name="value">The text to write</param>
        /// <param name="arg">Optional arguments for string formatting</param>
        private static void WriteBuildInfo(object value, params object[] arg)
        {
            Console.WriteLine("[BUILD] " + value.ToString(), arg);
        }

        /// <summary>
        /// Writes text with prefix '[DEPLOY]' to the console
        /// </summary>
        /// <param name="value">The text to write</param>
        /// <param name="arg">Optional arguments for string formatting</param>
        private static void WriteDeployInfo(object value, params object[] arg)
        {
            Console.WriteLine("[DEPLOY] " + value.ToString(), arg);
        }

        /// <summary>
        /// Writes text with prefix '[ERROR]' to the standard error output stream
        /// </summary>
        /// <param name="value">The text to write</param>
        /// <param name="arg">Optional arguments for string formatting</param>
        private static void WriteError(object value, params object[] arg)
        {
            Console.Error.WriteLine("[ERROR] " + value.ToString(), arg);
        }
    }
}
